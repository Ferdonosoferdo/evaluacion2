/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.eva.dao.AlumnosJpaController;
import root.eva.dao.exceptions.NonexistentEntityException;
import root.eva.entity.Alumnos;

/**
 *
 * @author gmesatica
 */
@WebServlet(name = "AlumnosController", urlPatterns = {"/AlumnosController"})
public class AlumnosController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AlumnosController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AlumnosController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
              AlumnosJpaController dao=new AlumnosJpaController();
        String accion=request.getParameter("accion");

        if (accion.equals("crear")){
        request.getRequestDispatcher("index.jsp").forward(request, response);
        } 
        
        if (accion.equals("eliminar")){
            try {
                String seleccion=request.getParameter("seleccion");
               
                dao.destroy(seleccion);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(AlumnosController.class.getName()).log(Level.SEVERE, null, ex);
            }
              List<Alumnos> listaAlumnos=dao.findAlumnosEntities();
         
        request.setAttribute("lista", listaAlumnos);
        
       request.getRequestDispatcher("listar.jsp").forward(request, response);
                
        }
        
        if (accion.equals("ver")){ 

            String seleccion=request.getParameter("seleccion");
            
             Alumnos alum= dao.findAlumnos(seleccion);
             
            request.setAttribute("alumnos", alum);
            request.getRequestDispatcher("editar.jsp").forward(request, response);      
        }            
        
        
        
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    String botonseleccionado =request.getParameter("accion");
        
        
         AlumnosJpaController dao=new AlumnosJpaController();
         
        try {
            String rut = request.getParameter("rut");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String edad = request.getParameter("edad");
            String seccion = request.getParameter("seccion");
            
            
            Alumnos alumnos=new Alumnos();
            alumnos.setRut(rut);
            alumnos.setNombres(nombres);
            alumnos.setApellidos(apellidos);
            alumnos.setEdad(edad);
            alumnos.setSeccion(seccion);
            
            
            
           if (botonseleccionado.equals("grabar")){
           
            dao.create(alumnos);
           }
           else{
               
             dao.edit(alumnos);
               
           }
           
            
         
        } catch (Exception ex) {
            Logger.getLogger(AlumnosController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        
        List<Alumnos> listaAlumnos=dao.findAlumnosEntities();
         
        request.setAttribute("lista", listaAlumnos);
        
       request.getRequestDispatcher("listar.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
