<%-- 
    Document   : listar
    Created on : 05-10-2021, 20:42:05
    Author     : gmesatica
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.eva.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Alumnos> lista = (List<Alumnos>) request.getAttribute("lista");

    Iterator<Alumnos> itlista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <link rel="stylesheet" href="estilos.css" >
    </head>
    
    <body>
  <div class="container text-center">
      
    <h1>Lista de Alumnos</h1>      
   
  </div>
          <div class="container table">

      <form  name="form" action="AlumnosController" method="GET">

        <table class="table" >
            <thead>
            <tr>
            <th>Rut</th>
            <th>Nombres </th>
            <th>Apellidos </th>
            <th>Edad  </th>
            <th>Sección</th>
            <th>Selecciona</th>
          </tr>
        </thead>
        <tbody>
            <%while (itlista.hasNext()) {
                   Alumnos alu = itlista.next();%>

            <tr>
                <td class="success"><%= alu.getRut()%></td> 
                <td class="success"><%= alu.getNombres()%></td> 
                <td class="success"><%= alu.getApellidos()%></td> 
                <td class="success"><%= alu.getEdad()%></td> 
                <td class="success"><%= alu.getSeccion()%></td> 
                <td> <input id="selec" type="radio" name="seleccion" required value="<%= alu.getRut()%>"> </td>
            </tr>
           <%}%>  


        </tbody>


        </table>
           <div class="container-fluid bg-3 text-center">   
               <button type="submit" name="accion" value="crear"  class="btn btn-default" 
                       formnovalidate="">Crear Alumno</button>
         <button type="submit" name="accion" value="ver" class="btn btn-default" >Editar Alumno</button>
         <button type="submit" name="accion" value="eliminar" class="btn btn-default" >Eliminar Alumno</button> 
          </div >   
     </form>
          </div>

        
    </body>
</html>
