<%-- 
    Document   : editar
    Created on : 05-10-2021, 20:48:45
    Author     : gmesatica
--%>

<%@page import="root.eva.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <%
    Alumnos alumnos = (Alumnos) request.getAttribute("alumnos");

%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
           <link rel="stylesheet" href="estilos.css" >
    </head>
     <body>
        <div class="container text-center">
      
    <h1>Editar Alumno</h1>      
   
  </div>
          <div class="container-fluid bg-3 text-center">  
          <form id="frm" name="form" action="AlumnosController" method="POST">
        
   <table class="table">
    <thead>
      <tr>
        <th>Rut</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Edad</th>
        <th>Sección</th>
      </tr>
    </thead>
    <tbody>
      <tr>
          <td><input name="rut" class="form-control" id="rut" 
                     title="Campo obligatorio" value="<%= alumnos.getRut()%>"></td>
        <td> <input name="nombres" class="form-control" id="nombres"
                    value="<%= alumnos.getNombres()%>"></td>
        <td><input  name="apellidos" class="form-control" id="apellidos"
           value="<%= alumnos.getApellidos()%>"          ></td>
        <td><input name="edad" class="form-control" id="edad"
                   value="<%= alumnos.getEdad()%>"></td>
        <td><input  name="seccion" class="form-control" id="seccion"
                    value="<%= alumnos.getSeccion()%>"></td>
      </tr> 
         </table>
         <div class="container-fluid bg-3 text-center">  
      <button type="submit" name="accion" id="grabar" value="grabareditar" class="btn btn-default" >
          Guardar Cambios
      </button>
         </div>
        </form>
          </div>   
   
     </body>                   
</html>
