<%-- 
    Document   : index
    Created on : 05-10-2021, 20:32:46
    Author     : gmesatica
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
          <link rel="stylesheet" href="estilos.css" >
    </head>
        <body>

    <section id="seccioninicio" >
        <div class="container text-center">
    <h1>Ingreso de Alumno</h1>      
    <p>Ingresa los datos del alumno o selecciona "listar" para ver la lista de alumnos </p>
  </div>
        <div class="container-fluid bg-3 text-center">   
   <form id="frm" name="form" action="AlumnosController" method="POST">
        
   <table class="table">
    <thead>
      <tr>
        <th>Rut</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Edad</th>
        <th>Sección</th>
      </tr>
    </thead>
    <tbody>
      <tr>
          <td><input name="rut" class="form-control" id="rut" placeholder="Rut" required 
                     title="Campo obligatorio"></td>
        <td> <input name="nombres" class="form-control" id="nombres" 
                    required placeholder="Nombres"></td>
        <td><input  name="apellidos" class="form-control" id="apellidos" 
                    required placeholder="Apellidos"></td>
        <td><input name="edad" class="form-control" id="edad" 
                   required placeholder="Edad"></td>
        <td><input  name="seccion" class="form-control" id="seccion"
                    required placeholder="Sección"></td>
      </tr> 
    </tbody>
         </table>
      <button type="submit" name="accion" value="grabar" 
              class="btn btn-default" >Ingresar datos</button>
         <br>
         <br>
          <div class="container text-center">
      <button type="submit"  name="accion" value="grabar" 
              class="btn btn-default" formnovalidate="">Listar alumnos</button>
          </div>

 
        </form>
            
        
  
        </body>
</html>
